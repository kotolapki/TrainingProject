import React from 'react';
import styled from 'styled-components/native';
import {defaultTheme} from 'theme/defaultTheme';

interface ButtonProps {
  text: string;
  onPress: () => void;
}

const Button = ({text, onPress}: ButtonProps) => {
  return (
    <Root onPress={onPress}>
      <Text>{text}</Text>
    </Root>
  );
};

export default Button;

const Root = styled.TouchableOpacity`
  padding: 7px 30px;
  background-color: ${defaultTheme.colors.accent};
  border-radius: 15px;
`;

const Text = styled.Text`
  text-align: center;
  color: ${defaultTheme.colors.white};
  ${defaultTheme.typography.headline}
`;
