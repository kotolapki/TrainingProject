import React from 'react';
import {Control, RegisterOptions, useController} from 'react-hook-form';
import {StyleProp, ViewStyle} from 'react-native';
import {KeyboardTypeOptions} from 'react-native';
import styled from 'styled-components/native';
import {defaultTheme} from 'theme/defaultTheme';

interface InputProps {
  label?: string;
  placeholder?: string;
  value?: string | null;
  disabled?: boolean;
  keyboardType?: KeyboardTypeOptions;
  name: string;
  control: Control<any>;
  rules?: RegisterOptions;
  style?: StyleProp<ViewStyle>;
  isTextArea?: boolean;
}

const Input = ({
  label,
  placeholder,
  disabled,
  keyboardType = 'default',
  name,
  control,
  rules,
  style,
  isTextArea = false,
}: InputProps) => {
  const {
    field: {...inputProps},
    fieldState: {invalid, error},
  } = useController({
    name,
    control,
    rules,
  });

  return (
    <Wrapper style={style}>
      {label && <Label $error={invalid && !!error?.message}>{label}</Label>}
      <StyledInput
        {...inputProps}
        $error={invalid && !!error?.message}
        onChangeText={inputProps.onChange}
        placeholder={placeholder}
        placeholderTextColor={defaultTheme.colors.grayLight}
        editable={!disabled}
        keyboardType={keyboardType}
        caretHidden={false}
        multiline={isTextArea}
        $isTextArea={isTextArea}
      />
      {invalid && error?.message && <Error>{error.message}</Error>}
    </Wrapper>
  );
};

const Wrapper = styled.View`
  background-color: ${defaultTheme.colors.transparent};
  position: relative;
  margin-bottom: 16px;
`;
const Label = styled.Text<{$error?: boolean}>`
  margin-bottom: 4px;
  color: ${({$error}) =>
    $error ? defaultTheme.colors.red : defaultTheme.colors.grayMiddle};
  ${defaultTheme.typography.footnote12}
`;

const StyledInput = styled.TextInput<{$error?: boolean; $isTextArea?: boolean}>`
  background-color: ${defaultTheme.colors.whiteGray};
  color: ${defaultTheme.colors.black};
  height: ${({$isTextArea}) => ($isTextArea ? '268px' : '46px')};
  padding: ${({$isTextArea}) =>
    $isTextArea ? '14px 16px' : '14px 48px 14px 16px'};
  border-radius: 12px;
  flex-direction: column;
  justify-content: flex-start;
  border: ${({$error}) =>
    $error
      ? `1px solid  ${defaultTheme.colors.red}`
      : `1px solid  ${defaultTheme.colors.whiteGray}`};
`;

const Error = styled.Text`
  position: absolute;
  bottom: -20px;
  right: 0;
  color: ${defaultTheme.colors.red};
  ${defaultTheme.typography.footnote12}
`;

export default Input;
