import SvgCheckBoxIcon from 'assets/svg/CheckBoxIcon';
import React from 'react';
import {StyleProp, ViewStyle} from 'react-native';
import styled from 'styled-components/native';
import {defaultTheme} from 'theme/defaultTheme';

interface CheckboxProps {
  value: boolean;
  style?: StyleProp<ViewStyle>;
}

const Checkbox = ({value, style}: CheckboxProps) => {
  return (
    <Root style={style}>
      <Container value={value}>{value && <SvgCheckBoxIcon />}</Container>
    </Root>
  );
};

export default Checkbox;

const Root = styled.View`
  padding: 5px;
`;

const Container = styled.View<{value: boolean}>`
  width: 16px;
  height: 16px;
  justify-content: center;
  align-items: center;
  border: ${`1px solid ${defaultTheme.colors.accent}`};
  border-radius: 4px;
  background-color: ${({value}) =>
    value ? defaultTheme.colors.accent : defaultTheme.colors.transparent};
`;
