import Checkbox from 'components/Checkbox';
import React from 'react';
import {StyleProp, TouchableOpacityProps, ViewStyle} from 'react-native';
import styled from 'styled-components/native';
import {defaultTheme} from 'theme/defaultTheme';

interface CheckboxFieldProps extends TouchableOpacityProps {
  label: string;
  value: boolean;
  onPress: () => void;
  isDisabled?: boolean;
  style?: StyleProp<ViewStyle>;
  activeOpacity?: number;
}

const CheckboxField = ({
  style,
  label,
  value,
  onPress,
  isDisabled,
  activeOpacity,
}: CheckboxFieldProps) => {
  return (
    <Container
      style={style}
      onPress={onPress}
      activeOpacity={activeOpacity || 0.5}
      disabled={isDisabled}
      $isDisabled={isDisabled}>
      <CheckboxWithMargin value={value} />
      <Label>{label}</Label>
    </Container>
  );
};

export default CheckboxField;

const Container = styled.TouchableOpacity<{$isDisabled?: boolean}>`
  padding: 16px;
  padding-left: 12px;
  flex-direction: row;
  align-items: center;
  border-radius: 16px;
  background-color: ${defaultTheme.colors.accentLight};
  opacity: ${({$isDisabled}) => ($isDisabled ? 0.5 : 1)};
`;

const CheckboxWithMargin = styled(Checkbox)`
  margin-right: 7px;
`;

const Label = styled.Text`
  color: ${defaultTheme.colors.black};
  ${defaultTheme.typography.baseText};
`;
