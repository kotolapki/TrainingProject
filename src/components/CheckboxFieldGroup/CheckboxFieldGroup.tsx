import CheckboxField from 'components/CheckboxField';
import React from 'react';
import {Control, RegisterOptions, useController} from 'react-hook-form';
import {StyleProp, View, ViewStyle} from 'react-native';
import styled from 'styled-components/native';
import {defaultTheme} from 'theme/defaultTheme';

type CheckboxFieldOption = {
  id: number | string;
  label: string;
  isDisabled?: boolean;
};

interface CheckboxFieldGroupProps {
  name: string;
  control: Control<any>;
  options: CheckboxFieldOption[];
  style?: StyleProp<ViewStyle>;
  customFieldStyle?: StyleProp<ViewStyle>;
  rules?: RegisterOptions;
}

const CheckboxFieldGroup = ({
  name,
  control,
  options,
  style,
  rules,
  customFieldStyle,
}: CheckboxFieldGroupProps) => {
  const {
    field: {value, onChange},
    fieldState: {invalid, error},
  } = useController({
    name,
    control,
    rules,
    defaultValue: [],
  });

  const handlePress = (id: number | string) => {
    const newValue = value.includes(id)
      ? value.filter((item: number | string) => String(item) !== String(id))
      : [...value, id];

    onChange(newValue);
  };

  return (
    <View style={style}>
      {options.map((item, index, array) => {
        const isLast = array.length - 1 === index;

        return (
          <StyledCheckboxField
            key={item.id}
            label={item.label}
            value={value?.includes(item.id)}
            onPress={() => handlePress(item.id)}
            isDisabled={item.isDisabled}
            isLast={isLast}
            style={customFieldStyle}
          />
        );
      })}
      {invalid && error?.message && <Error>{error.message}</Error>}
    </View>
  );
};

export default CheckboxFieldGroup;

const StyledCheckboxField = styled(CheckboxField)<{isLast?: boolean}>`
  margin-bottom: ${({isLast}) => (isLast ? 0 : '8px')};
`;

const Error = styled.Text`
  color: ${defaultTheme.colors.red};
  ${defaultTheme.typography.footnote12}
  text-align: center;
`;
