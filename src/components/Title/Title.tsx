import React from 'react';
import styled from 'styled-components/native';
import {defaultTheme} from 'theme/defaultTheme';

interface TitleProps {
  text: string;
}

const Title = ({text}: TitleProps) => {
  return <Root>{text}</Root>;
};

export default Title;

const Root = styled.Text`
  margin-bottom: 25px;
  text-align: center;
  color: ${defaultTheme.colors.accent};
  ${defaultTheme.typography.title20}
`;
