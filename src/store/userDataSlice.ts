import {createSlice, PayloadAction} from '@reduxjs/toolkit';

type InitialState = {
  name: string;
  gender: string;
  age: number | undefined;
  goals: string[];
};

const initialState = {
  name: '',
  gender: '',
  age: undefined,
  goals: [],
} as InitialState;

export const userDataSlice = createSlice({
  name: 'firstStep',
  initialState,
  reducers: {
    setFirstStepData(
      state,
      action: PayloadAction<{
        name: string;
        age: number;
      }>,
    ) {
      const {name, age} = action.payload;
      state.name = name;
      state.age = age;
    },
    setSecondStepData(
      state,
      action: PayloadAction<{
        gender: string;
        goals: string[];
      }>,
    ) {
      const {gender, goals} = action.payload;
      state.gender = gender;
      state.goals = goals;
    },
    resetData(state) {
      state.name = initialState.name;
      state.age = initialState.age;
      state.gender = initialState.gender;
      state.goals = initialState.goals;
    },
  },
});

export const {setFirstStepData, setSecondStepData, resetData} =
  userDataSlice.actions;
export default userDataSlice.reducer;
