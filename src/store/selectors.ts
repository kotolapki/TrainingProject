import {StateType} from 'store/store';

export const selectUserData = (state: StateType) => state.userData;
