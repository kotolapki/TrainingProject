import {configureStore} from '@reduxjs/toolkit';

import userDataSlice from './userDataSlice';

const reducer = {userData: userDataSlice};

export const store = configureStore({reducer});

export type StateType = ReturnType<typeof store.getState>;
