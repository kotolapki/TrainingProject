import React from 'react';
import {NavigationContainer, RouteProp} from '@react-navigation/native';
import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import StartScreen from 'navigation/StartScreen';
import FirstStep from 'navigation/FirstStep';
import SecondStep from 'navigation/SecondStep';
import ResultScreen from 'navigation/ResultScreen';
import 'i18n';
import {Screens} from 'navigation/routes';
import {Provider} from 'react-redux';
import {store} from 'store/store';

const Stack = createStackNavigator<NavigatorParams>();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name={Screens.StartScreen} component={StartScreen} />
          <Stack.Screen name={Screens.FirstStep} component={FirstStep} />
          <Stack.Screen name={Screens.SecondStep} component={SecondStep} />
          <Stack.Screen name={Screens.ResultScreen} component={ResultScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

type NavigatorParams = {
  [Screens.StartScreen]: undefined;
  [Screens.FirstStep]: {repeat?: boolean};
  [Screens.SecondStep]: undefined;
  [Screens.ResultScreen]: undefined;
};

export interface StartScreenProps {
  navigation: StackNavigationProp<NavigatorParams, Screens.StartScreen>;
}

export interface FirstStepProps {
  navigation: StackNavigationProp<NavigatorParams, Screens.FirstStep>;
  route: RouteProp<NavigatorParams, Screens.FirstStep>;
}

export interface SecondStepProps {
  navigation: StackNavigationProp<NavigatorParams, Screens.SecondStep>;
}

export interface ResultScreenProps {
  navigation: StackNavigationProp<NavigatorParams, Screens.ResultScreen>;
}

export default App;
