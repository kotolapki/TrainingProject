import React, {useState, useMemo} from 'react';

import styled from 'styled-components/native';
import MainLayout from 'layouts/MainLayout';
import {useTranslation} from 'react-i18next';
import {defaultTheme} from 'theme/defaultTheme';
import {useForm, Controller} from 'react-hook-form';
import Button from 'components/Button';
import Title from 'components/Title';
import {useSelector, useDispatch} from 'react-redux';
import {selectUserData} from 'store/selectors';
import CheckboxFieldGroup from 'components/CheckboxFieldGroup';
import DropDownPicker from 'react-native-dropdown-picker';
import {Gender} from 'types/common';
import {useCheckboxOptionsByGender} from 'hooks/useCheckboxOptionsByGender';
import {setSecondStepData} from 'store/userDataSlice';
import {SecondStepProps} from 'App';
import {Screens} from 'navigation/routes';

interface SecondStepValues {
  gender: string;
  goals: string[];
}

const SecondStep = ({navigation}: SecondStepProps) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const userData = useSelector(selectUserData);
  const [isSelectOpen, setIsSelectOpen] = useState(false);

  const preparedDefaultValues = {
    gender: userData.gender,
    goals: userData.goals,
  };

  const {control, handleSubmit, watch} = useForm({
    defaultValues: preparedDefaultValues,
  });

  const {male, female, attackHelicopter} = useMemo(
    () => t('inputs.genderType', {returnObjects: true}),
    [t],
  );

  const genderOptions = useMemo(() => {
    return [
      {label: male, value: Gender.male},
      {label: female, value: Gender.female},
      {label: attackHelicopter, value: Gender.attackHelicopter},
    ];
  }, [male, female, attackHelicopter]);

  const genderField = watch('gender');

  const getGender = () => {
    switch (genderField) {
      case 'male':
        return Gender.male;
      case 'female':
        return Gender.female;
      case 'attackHelicopter':
        return Gender.attackHelicopter;
      default:
        return;
    }
  };

  const options = useCheckboxOptionsByGender(getGender());

  const onSubmit = (values: SecondStepValues) => {
    dispatch(setSecondStepData(values));
    navigation.navigate(Screens.ResultScreen);
  };

  return (
    <MainLayout>
      <Title text={`${t('screens.secondStep.title')}, ${userData.name}`} />
      <ContentWrapper>
        <Controller
          name="gender"
          control={control}
          rules={{required: t('validation.required')}}
          render={({field, fieldState}) => {
            const {error, invalid} = fieldState;

            return (
              <>
                <DropDownPicker
                  zIndex={1000}
                  open={isSelectOpen}
                  setOpen={setIsSelectOpen}
                  items={genderOptions}
                  value={field.value}
                  setValue={value => field.onChange(value)}
                  onChangeValue={() => field.onChange(field.value)}
                  placeholder={t('inputs.gender')}
                  style={{
                    marginBottom: 25,
                    borderColor: invalid
                      ? defaultTheme.colors.red
                      : defaultTheme.colors.accent,
                  }}
                />
                {invalid && !!error?.message && <Error>{error.message}</Error>}
              </>
            );
          }}
        />
        {options && (
          <CheckboxFieldGroup
            name="goals"
            control={control}
            rules={{required: t('validation.required')}}
            options={options}
          />
        )}
      </ContentWrapper>
      <Button text={t('buttons.submit')} onPress={handleSubmit(onSubmit)} />
    </MainLayout>
  );
};

export default SecondStep;

const ContentWrapper = styled.View`
  flex-grow: 1;
`;

const Error = styled.Text`
  position: absolute;
  bottom: -20px;
  right: 0;
  color: ${defaultTheme.colors.red};
  ${defaultTheme.typography.footnote12}
`;
