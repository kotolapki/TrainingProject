import React from 'react';
import styled from 'styled-components/native';
import MainLayout from 'layouts/MainLayout';
import Title from 'components/Title';
import Button from 'components/Button';
import {useTranslation} from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';
import {selectUserData} from 'store/selectors';
import {resetData} from 'store/userDataSlice';
import {ResultScreenProps} from 'App';
import {Screens} from 'navigation/routes';
import {View} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {defaultTheme} from 'theme/defaultTheme';

const ResultScreen = ({navigation}: ResultScreenProps) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const userData = useSelector(selectUserData);
  const userDataPrepared = Object.entries(useSelector(selectUserData));
  const inputs = t('inputs', {returnObjects: true});
  const goals = t('goals', {returnObjects: true});

  const handleRepeatPress = () => {
    dispatch(resetData());
    navigation.replace(Screens.FirstStep, {repeat: true});
  };

  const getValueByLabel = (label: string, value: string | string[]) => {
    if (label === 'gender' && typeof value === 'string') {
      //@ts-ignore
      return inputs.genderType[value];
    } else if (label === 'goals' && typeof value === 'object') {
      const values = value.map((item, index, array) => {
        //@ts-ignore
        return `${goals[userData.gender][item]}${
          array.length === index + 1 ? '' : ', '
        }`;
      });

      return values;
    } else {
      return value;
    }
  };

  return (
    <MainLayout>
      <View>
        <Title text={t('screens.resultScreen.title')} />
        <FlatList
          data={userDataPrepared}
          keyExtractor={item => item[0]}
          renderItem={({item}) => {
            //@ts-ignore
            const label = inputs[item[0]];
            const value = item[1];

            return (
              <Row>
                <ResultCategory>{label}</ResultCategory>
                <ResultValue>{getValueByLabel(item[0], value)}</ResultValue>
              </Row>
            );
          }}
        />
      </View>
      <Button text={t('buttons.again')} onPress={handleRepeatPress} />
    </MainLayout>
  );
};

export default ResultScreen;

const Row = styled.View`
  margin-bottom: 20px;
  flex-direction: row;
  justify-content: space-between;
`;

const ResultCategory = styled.Text`
  color: ${defaultTheme.colors.accentDark};
  ${defaultTheme.typography.baseText}
`;

const ResultValue = styled.Text`
  color: ${defaultTheme.colors.black};
  ${defaultTheme.typography.baseText}
`;
