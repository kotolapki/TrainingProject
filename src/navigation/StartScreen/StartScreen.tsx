import React from 'react';

import styled from 'styled-components/native';
import MainLayout from 'layouts/MainLayout';
import {useTranslation} from 'react-i18next';
import {Screens} from 'navigation/routes';
import Button from 'components/Button';
import Title from 'components/Title';

import {StartScreenProps} from 'App';

const StartScreen = ({navigation}: StartScreenProps) => {
  const {t} = useTranslation();

  return (
    <MainLayout>
      <Container>
        <Title text={t('screens.startScreen.title')} />
        <Button
          text={t('buttons.start')}
          onPress={() => navigation.navigate(Screens.FirstStep, {})}
        />
      </Container>
    </MainLayout>
  );
};

export default StartScreen;

const Container = styled.View`
  flex-grow: 1;
  align-items: center;
  justify-content: center;
`;
