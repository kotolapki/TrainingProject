import React, {useEffect} from 'react';

import styled from 'styled-components/native';
import MainLayout from 'layouts/MainLayout';
import {useTranslation} from 'react-i18next';
import {useForm} from 'react-hook-form';
import Button from 'components/Button';
import Title from 'components/Title';
import {useSelector, useDispatch} from 'react-redux';
import {selectUserData} from 'store/selectors';
import Input from 'components/Input';
import {setFirstStepData} from 'store/userDataSlice';
import {FirstStepProps} from 'App';
import {Screens} from 'navigation/routes';
import {View} from 'react-native';

interface FirstStepValues {
  name: string;
  age: number;
}

const FirstStep = ({navigation, route}: FirstStepProps) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const repeat = route.params.repeat;
  const userData = useSelector(selectUserData);
  const preparedValue = {
    name: userData.name,
    age: '' || userData.age?.toString(),
  };

  const {name, age} = t('inputs', {returnObjects: true});

  const {handleSubmit, control, reset} = useForm({
    defaultValues: preparedValue,
  });

  const onSubmit = (values: FirstStepValues) => {
    dispatch(setFirstStepData(values));
    reset();
    navigation.navigate(Screens.SecondStep);
  };

  useEffect(() => {
    if (repeat) {
      reset();
    }
  }, [repeat, reset]);

  return (
    <MainLayout isKeyboardAvoided>
      <View>
        <Title text={t('screens.firstStep.title')} />
        <StyledInput name="name" label={name} control={control} />
        <StyledInput
          name="age"
          label={age}
          keyboardType="numeric"
          control={control}
          rules={{
            required: t('validation.required'),
            min: {value: 18, message: t('validation.min')},
          }}
        />
      </View>
      <Button text={t('buttons.submit')} onPress={handleSubmit(onSubmit)} />
    </MainLayout>
  );
};

export default FirstStep;

const StyledInput = styled(Input)`
  margin-bottom: 15px;
  width: 100%;
`;
