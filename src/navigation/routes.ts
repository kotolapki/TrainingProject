export enum Screens {
  StartScreen = 'StartScreen',
  FirstStep = 'FirstStep',
  SecondStep = 'SecondStep',
  ResultScreen = 'ResultScreen',
}
