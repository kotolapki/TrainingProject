import 'react-i18next';

import Russian from 'assets/lang/ru.json';

declare module 'react-i18next' {
  interface CustomTypeOptions {
    defaultNS: 'ru';
    resources: {
      ru: typeof Russian;
    };
  }
}
