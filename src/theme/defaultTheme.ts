import fonts from './fonts';

export const defaultTheme = {
  name: 'default',
  colors: {
    transparent: 'transparent',
    black: '#11263C',
    gray: '#586777',
    grayDark: '#938FAE',
    grayMiddle: '#88939E',
    grayLight: '#B8BEC5',
    grayLighter: '#E7E9EC',
    whiteGray: '#F5F5F6',
    white: '#ffffff',
    accent: '#5643F0',
    accentLight: '#F4F3FF',
    accentMedium: '#5C4AF4',
    accentDark: '#4F3CEB',
    tertiatry: '#C1BDE7',
    blue: '#4166EB',
    lavender: '#7469D9',
    indigo: '#2513C2',
    smoky: '#4D66BE',
    lilac: '#A89FF7',
    red: '#DA3749',
    pink: '#FF4593',
    disabled: '#E4E1FD',
    opacity: 'rgba(255, 255, 255, 0.2)',
    grayBorder: '#F0F0F1',
  },
  typography: {
    title48: {
      fontSize: 48,
      lineHeight: '57px',
      ...fonts.medium,
    },
    title34: {
      fontSize: 34,
      lineHeight: '50px',
      ...fonts.medium,
    },
    title34Bold: {
      fontSize: 34,
      lineHeight: '50px',
      ...fonts.bold,
    },
    title28: {
      fontSize: 28,
      lineHeight: '34px',
      ...fonts.medium,
    },
    title24: {
      fontSize: 24,
      lineHeight: '32px',
      ...fonts.medium,
    },
    title20: {
      fontSize: 20,
      lineHeight: '25px',
      ...fonts.medium,
    },
    headline: {
      fontSize: 17,
      lineHeight: '32px',
      ...fonts.medium,
    },
    baseText: {
      fontSize: 16,
      lineHeight: '21px',
      ...fonts.regular,
    },
    baseMedium: {
      fontSize: 16,
      lineHeight: '21px',
      ...fonts.medium,
    },
    footnote: {
      fontSize: 14,
      lineHeight: '21px',
      ...fonts.regular,
    },
    footnoteMedium: {
      fontSize: 14,
      lineHeight: '21px',
      ...fonts.medium,
    },
    footnote12: {
      fontSize: 12,
      lineHeight: '20px',
      ...fonts.regular,
    },
    footnote12Medium: {
      fontSize: 12,
      lineHeight: '20px',
      ...fonts.medium,
    },
    caption: {
      fontSize: 10,
      lineHeight: '20px',
      ...fonts.regular,
    },
    captionMedium: {
      fontSize: 10,
      lineHeight: '20px',
      ...fonts.medium,
    },
    captionThin: {
      fontSize: 10,
      lineHeight: '12px',
      ...fonts.regular,
    },
    captionThinMedium: {
      fontSize: 10,
      lineHeight: '12px',
      ...fonts.medium,
    },
  },
};
