import {Platform} from 'react-native';

//@ts-ignore
const bold = Platform.select({
  ios: () => {
    return {fontFamily: 'Rubik-Bold', fontWeight: '700'};
  },
  android: () => {
    return {fontFamily: 'Rubik-Bold'};
  },
})();
//@ts-ignore
const semibold = Platform.select({
  ios: () => {
    return {fontFamily: 'Rubik-SemiBold', fontWeight: '600'};
  },
  android: () => {
    return {fontFamily: 'Rubik-SemiBold'};
  },
})();
//@ts-ignore
const regular = Platform.select({
  ios: () => {
    return {fontFamily: 'Rubik-Regular', fontWeight: '400'};
  },
  android: () => {
    return {fontFamily: 'Rubik-Regular'};
  },
})();
//@ts-ignore
const medium = Platform.select({
  ios: () => {
    return {fontFamily: 'Rubik-Medium', fontWeight: '500'};
  },
  android: () => {
    return {fontFamily: 'Rubik-Medium'};
  },
})();
//@ts-ignore
const light = Platform.select({
  ios: () => {
    return {fontFamily: 'Rubik-Light', fontWeight: '300'};
  },
  android: () => {
    return {fontFamily: 'Rubik-Light'};
  },
})();

export default {bold, semibold, regular, medium, light};
