import React from 'react';
import styled from 'styled-components/native';
import {useSafeAreaInsets, EdgeInsets} from 'react-native-safe-area-context';
import {Platform} from 'react-native';

interface MainLayoutProps {
  children: React.ReactNode;
  isKeyboardAvoided?: boolean;
}

const MainLayout = ({children, isKeyboardAvoided}: MainLayoutProps) => {
  const insets = useSafeAreaInsets();

  return (
    <Root $insets={insets}>
      {isKeyboardAvoided ? (
        <KeyBoardAvoid
          behavior={Platform.OS === 'ios' ? 'padding' : undefined}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 95 : 0}>
          <ScroolViewContainer
            bounces={false}
            showsVerticalScrollIndicator={false}>
            {children}
          </ScroolViewContainer>
        </KeyBoardAvoid>
      ) : (
        <ViewContainer>{children}</ViewContainer>
      )}
    </Root>
  );
};

export default MainLayout;

const Root = styled.View<{$insets: EdgeInsets}>`
  padding: ${({$insets}) =>
    `0px ${$insets.right}px ${$insets.bottom}px ${$insets.left}px`};
  flex: 1;
  background-color: white;
`;

const KeyBoardAvoid = styled.KeyboardAvoidingView.attrs(() => ({
  contentContainerStyle: {
    flex: 1,
  },
}))`
  flex: 1;
`;

const ScroolViewContainer = styled.ScrollView.attrs(() => ({
  contentContainerStyle: {
    flexGrow: 1,
    justifyContent: 'space-between',
  },
}))`
  padding: 20px 16px 10px;
`;

const ViewContainer = styled.View`
  padding: 20px 16px 10px;
  flex-grow: 1;
  justify-content: space-between;
`;
