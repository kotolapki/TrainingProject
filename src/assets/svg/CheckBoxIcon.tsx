import * as React from 'react';
import Svg, {Path, SvgProps} from 'react-native-svg';

function SvgCheckBoxIcon(props: SvgProps) {
  return (
    <Svg width={9} height={8} fill="none" {...props}>
      <Path d="M1 4.375L3.667 7 8.2.875" stroke="#fff" strokeLinecap="round" />
    </Svg>
  );
}

export default SvgCheckBoxIcon;
