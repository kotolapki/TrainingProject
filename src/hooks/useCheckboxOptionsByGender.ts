import {Gender} from 'types/common';
import {useTranslation} from 'react-i18next';
import {useMemo} from 'react';

export const useCheckboxOptionsByGender = (gender: Gender | undefined) => {
  const {t} = useTranslation();
  const {male, female, attackHelicopter} = t('goals', {returnObjects: true});

  const options = useMemo(
    () => ({
      male: [
        {
          label: male.drinkBeer,
          id: 'drinkBeer',
        },
        {
          label: male.smoking,
          id: 'smoking',
        },
      ],
      female: [
        {
          label: female.dress,
          id: 'dress',
        },
        {
          label: female.flowers,
          id: 'flowers',
        },
      ],
      attackHelicopter: [
        {
          label: attackHelicopter.attack,
          id: 'attack',
        },
        {
          label: attackHelicopter.fly,
          id: 'fly',
        },
      ],
    }),
    [
      attackHelicopter.attack,
      attackHelicopter.fly,
      female.dress,
      female.flowers,
      male.drinkBeer,
      male.smoking,
    ],
  );

  switch (gender) {
    case Gender.male: {
      return options.male;
    }
    case Gender.female: {
      return options.female;
    }
    case Gender.attackHelicopter: {
      return options.attackHelicopter;
    }
    default:
      return;
  }
};
